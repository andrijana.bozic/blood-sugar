//
//  RecordingSugarView.swift
//  BloodSugar
//
//  Created by student on 02.02.2024..
//

import SwiftUI

struct RecordingSugarView: View {
    
    @State private var bloodSugarLevel: Double = UserDefaults.standard.double(forKey: "bloodSugarLevel")
    @State private var savedBloodSugarLevels: [String] = UserDefaults.standard.array(forKey: "savedBloodSugarLevels") as? [String] ?? []

    var body: some View {
        VStack{
            HStack{
                Spacer()
                
                Text("Now you can enter values of your blood sugar level")
                    .navigationBarTitle("", displayMode: .inline)
                    .font(.title3)
                    .multilineTextAlignment(.center)
                Spacer()
            }
            .padding(.top, 60)

            TextField("Enter value", value: $bloodSugarLevel, formatter: NumberFormatter())
                .padding()
                .textFieldStyle(RoundedBorderTextFieldStyle())
                .frame(width: 110)
                .padding(.bottom, 15)
                .keyboardType(.decimalPad)
            
            Button("Save") {
                if bloodSugarLevel != 0 {
                    saveBloodSugarLevel()
                }
            }
            .frame(width: 80, height: 30)
            .foregroundColor(.black)
            .background(Color.white)
            .cornerRadius(15)
            .font(.headline)
            
            if !savedBloodSugarLevels.isEmpty {
                ScrollView(.vertical, showsIndicators: true) {
                    VStack(alignment: .center, spacing: 10) {
                        
                        Text("Normal blood sugar concentration is between 3.8 and 5.6 mmol/L")
                            .font(.title3)
                            .foregroundColor(.black)
                            .fontWeight(.medium)
                        
                        Text("Last blood sugar levels:")
                            .font(.title3)
                            .foregroundColor(.red)
                            .fontWeight(.bold)
                        
                        ForEach(savedBloodSugarLevels.reversed(), id: \.self) { level in
                            HStack {
                                Spacer()
                                Text("Blood Sugar level is: \(level) mmol/L.")
                                Spacer()
                                Button(action: {
                                    deleteBloodSugarLevel(Double(level) ?? 0)
                                }) {
                                    Image(systemName: "trash")
                                        .foregroundColor(.black)
                                }
                                Spacer()
                            }
                        }
                    }
                }
            }

            Spacer()
        }
        .background(Color.green)
    }

    private func saveBloodSugarLevel() {
        savedBloodSugarLevels.append(String(bloodSugarLevel))
        bloodSugarLevel = 0.0
        
        UserDefaults.standard.set(bloodSugarLevel, forKey: "bloodSugarLevel")
        UserDefaults.standard.set(savedBloodSugarLevels, forKey: "savedBloodSugarLevels")
    }
    
    private func deleteBloodSugarLevel(_ level: Double) {
        if let index = savedBloodSugarLevels.firstIndex(of: String(level)) {
            savedBloodSugarLevels.remove(at: index)
            UserDefaults.standard.set(savedBloodSugarLevels, forKey: "savedBloodSugarLevels")
        }
    }
}


