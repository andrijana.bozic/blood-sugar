//
//  BloodSugarApp.swift
//  BloodSugar
//
//  Created by student on 23.01.2024..
//

import SwiftUI

@main
struct BloodSugarApp: App {
    var body: some Scene {
        WindowGroup {
            TabView {
                ContentView()
                    .tabItem {
                        Label("Home Page", systemImage: "house.fill")
                    }
                RecordingSugarView()
                    .tabItem {
                        Label("Recording Sugar", systemImage: "drop.fill")
                    }
                    
            }
        }
        
    }
    
}
struct BloodSugarView_Previews: PreviewProvider {
    static var previews: some View {
        RecordingSugarView()
    }
}
