//
//  ContentView.swift
//  BloodSugar
//
//  Created by student on 23.01.2024..
//

import SwiftUI

struct ContentView: View {
    
    var body: some View {
            VStack {
                Spacer()
                Text("Blood Sugar")
                    .font(.system(size: 60))
                    .padding(.top, 60)
                Spacer()
                Text("Recording my")
                    .font(.system(size: 30))
                Text("blood sugar")
                    .font(.system(size: 30))
                Image("blood")
                
                Spacer()
            }
            .background(Color.green)
        
    }
    
}

#Preview {
    ContentView()
}
